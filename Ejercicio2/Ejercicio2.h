
#ifndef EJERCICIO2_H
#define EJERCICIO2_H
#include <iostream>
using namespace std;

class Ejercicio2{
    private:
    int n = 0;
    string *frases = NULL;

    public:
    Ejercicio2(int n, string *frases);

    // Getters y Setters
    string getFrases();
    int getN();

    void setFrases(string *frases, int n);
    void setN(int n);
    void conteoMayusMinus(string *frases, int n);
};
#endif