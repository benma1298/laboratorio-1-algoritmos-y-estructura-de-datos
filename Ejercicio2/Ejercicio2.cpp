
#include <iostream>
#include <string.h>
#include <ctype.h>
#include "Ejercicio2.h"
using namespace std;

//Constructor
Ejercicio2::Ejercicio2(int n, string *frases){
    this -> n = n;
    this -> frases = frases;
}

//Getters y Setters
string Ejercicio2::getFrases(){
    return this -> frases[n];
}
int Ejercicio2::getN(){
    return this -> n;
}

void Ejercicio2::setFrases(string *frases, int n){
    this -> frases[n] = frases[n];
}
void Ejercicio2::setN(int n){
    this -> n = n;
}

//Metodo

// Este metodo calcula la longitud del arreglo frases[]
// y la cantidad de letras minusculas y mayusculas por medio
// de las funciones islower() y isupper() de la libreria cctype.
void Ejercicio2::conteoMayusMinus(string *frases, int n){
    int cantidadMayus = 0;
    int cantidadMinus = 0;
    for(int i = 0; i <= n; i++){
        int length = frases[i].length();
        int contadorMayus = 0;
        int contadorMinus = 0;
        cout << frases[i] << endl;
        for(int x = 0; x < length ; x++){
            if(islower(frases[i][x])){
                contadorMinus++;
            }
            else if(isupper(frases[i][x])){
                contadorMayus++;
            }
        }
        cout << "La frase " << i << " posee " << contadorMayus << " letras Mayusculas y " << contadorMinus << " letras Minusculas. \n" << endl;
        cantidadMayus = cantidadMayus + contadorMayus;
        cantidadMinus = cantidadMinus + contadorMinus;
    }
    cout << "En total se han contado " << cantidadMayus << " letras Mayusculas y " << cantidadMinus << " letras Minusculas. \n" << endl;
}