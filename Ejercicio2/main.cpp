
#include <iostream>
#include <string.h>
#include <ctype.h>
#include "Ejercicio2.h"
using namespace std;

int main(){
    int n;
    string frases[n];

    //El usuario define la cantidad de frases que agregara por la terminal.
    cout << "Ingrese la cantidad de frases que desea agregar:" << endl;
    cin >> n;
    cout << "A continuacion ingrese " << n << " frases:" << endl;
    Ejercicio2 ejercicio2 = Ejercicio2(n, frases);

    //Ciclo for para almacenar las frases en el arreglo frases[].
    for(int i = 0; i <= n; i++){
        cout << "\n";
        getline(cin, frases[i]);
    }
    ejercicio2.conteoMayusMinus(frases, n);

    return 0;
}