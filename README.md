# Laboratorio 1 Algoritmos y Estructura de Datos

Autor: Benjamin Esteban Martin Albornoz.
Fecha: Lunes 6 de Septiembre.

Se escribieron los programas en C++ para los respectivos problemas del Laboratorio 1
Se usaron diversas clases para cada problema ademas de su Makefile correspondiente para compilar.

Ejercicio 1:
El programa consta de 3 clases: main.cpp, Ejercicio1.cpp y Ejercicio1.h ademas de su Makefile.
Se llena un arreglo unidimensional de N numeros enteros (cantidad a eleccion del usuario, pedida
por el programa en la terminal), se calculan los cuadrados de cada numero ingresado mediante un ciclo for
multiplicando el espacio del numero en el arreglo por si mismo y se suman los cuadrados entregando asi la suma total.

Ejercicio2:
El programa consta de 3 clases: main.cpp, Ejercicio2.cpp y Ejercicio2.h ademas de su Makefile.
Se llena un arreglo de caracteres con N frases (Tanto la cantidad de frases como las frases, son definidas por el
usuario mediante la terminal), y se calcula la cantidad de letras Minusculas y Mayusculas que posee cada frase ingresada.

Ejercicio3:
El programa consta de 3 clases: main.cpp, Ejercicio3.cpp y Ejercicio3.h ademas de su Makefile.
El programa guarda los datos de N clientes que se deseen agregar (El usuario define la cantidad de clientes)
Los datos de los clientes que se almacenan en el programa son: Nombre(string), Numero(string), Saldo(int) y Moroso(boleano).
Finalizado el ingreso de datos, se imprime en la terminal los datos de todos los clientes ingresados previamente. 
