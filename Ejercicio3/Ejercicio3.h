
#ifndef EJERCICIO3_H
#define EJERCICIO3_H
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

class Ejercicio3{
    private:
    string nombre;
    string numero;
    int saldo;
    bool morosidad = true;

    public:
    // Contructor
    Ejercicio3();

    // Getters y Setters
    string getNombre();
    string getNumero();
    int getSaldo();
    bool getMorosidad();

    void setNombre(string nombre);
    void setNumero(string numero);
    void setSaldo(int saldo);
    void setMorosidad(bool morosidad);
    void registroClientes();
};

#endif