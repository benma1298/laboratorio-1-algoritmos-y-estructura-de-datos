
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Ejercicio3.h"
using namespace std;

// Metodos

// Este metodo guarda los datos de Nombre, Telefono, Saldo y Moroso,
// al cliente agregado al arreglo personas[]
Ejercicio3 addCliente(Ejercicio3 ejercicio3){
    cout << "Ingrese los datos del cliente que sea agregar: \n" << endl;
    string nombre;
    string numero;
    int saldo;
    int moroso;
    bool morosidad = true;

    cout << "Nombre: \n";
    getline(cin, nombre);
    ejercicio3.setNombre(nombre);
    
    cout << "Telefono: \n";
    getline(cin, numero);
    ejercicio3.setNumero(numero);
    
    cout << "Saldo: \n";
    cin >> saldo;
    ejercicio3.setSaldo(saldo);

    cout << "Moroso: \n";
    cout << "Ingrese <0> si NO es Moroso; Ingrese <1> si es Moroso" << endl;
    cin >> moroso;
    cin.ignore();
    if(moroso == 1){
        ejercicio3.setMorosidad(morosidad);
    }else{
        ejercicio3.setMorosidad(morosidad = false);
    }
    return ejercicio3;
};

// Este Metodo agrega los datos de cada cliente al arreglo personas[].
void addPersona(Ejercicio3 personas[], int n){
    for(int i = 0; i < n; i++){
        personas[i] = Ejercicio3();
        personas[i] = addCliente(personas[i]);
    }
}

// Este metodo imprime los datos de los clientes que han sido guardados.
void imprimirRegistros(Ejercicio3 personas[],int n){
    for(int i = 0; i < n; i++){
        cout << "Cliente " << i+1 << endl;
        personas[i].registroClientes();
    }
}

int main(){
    int n;
    // Se ingresa la cantidad de clientes que el usuario desea agregar, por la terminal.
    cout << "Ingrese la cantidad de clientes que desea agregar: ";
    cin >> n;
    cin.ignore();

    Ejercicio3 personas[n];
    addPersona(personas, n);
    imprimirRegistros(personas, n);

    return 0;
}
