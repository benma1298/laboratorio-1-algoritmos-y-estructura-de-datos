
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "Ejercicio3.h"
using namespace std;

// Constructor
Ejercicio3::Ejercicio3(){}

// Getters y Setters
string Ejercicio3::getNombre(){
    return this -> nombre;
}
string Ejercicio3::getNumero(){
    return this -> numero;
}
int Ejercicio3::getSaldo(){
    return this -> saldo;
}
bool Ejercicio3::getMorosidad(){
    if(morosidad == true){
        cout << "El cliente es Moroso.";
    }else{
        cout << "El cliente no es Moroso.";
    }
}

void Ejercicio3::setNombre(string nombre){
    this -> nombre = nombre;
}
void Ejercicio3::setNumero(string numero){
    this -> numero = numero;
}
void Ejercicio3::setSaldo(int saldo){
    this -> saldo = saldo;
}
void Ejercicio3::setMorosidad(bool morosidad){
    this -> morosidad = morosidad;
}
void Ejercicio3::registroClientes(){
    cout << "Nombre: " << nombre << endl;
    cout << "Telefono: " << numero << endl;
    cout << "Saldo: " << saldo << endl;
    if(morosidad == 1){
        cout << "Cliente Moroso." << endl;
    }else{
        cout << "Cliente NO Moroso." << endl;
    }
}