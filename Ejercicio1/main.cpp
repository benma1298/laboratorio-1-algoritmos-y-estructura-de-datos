
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Ejercicio1.h"
using namespace std;

int main(){
    int n;
    int numeros[n];

    //El usuario define por la terminal la cantidad de numeros.
    cout << "Ingrese la cantidad de Numeros que desea agregar: \n";
    cin >> n;

    Ejercicio1 ejercicio1 = Ejercicio1(n, numeros);

    ejercicio1.addNumeros(numeros, n);
    ejercicio1.sumaCuadrados(numeros, n);

    return 0;
}