
#ifndef EJERCICIO1_H
#define EJERCICIO1_H
#include <iostream>
#include <stdlib.h>
using namespace std;

class Ejercicio1{

    private:
    int n = 0;
    int *numeros = NULL;

    public:
    Ejercicio1(int n, int *arreglo);

    // Getters y Setters
    int getN();
    int getNumeros();

    void setN(int n);
    void setNumeros(int *numeros, int n);

    // Metodos
    void addNumeros(int *numeros, int n);
    void sumaCuadrados(int *numeros, int n);
};
#endif