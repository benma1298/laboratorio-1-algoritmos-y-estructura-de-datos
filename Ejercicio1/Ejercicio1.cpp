
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Ejercicio1.h"
using namespace std;

//Constructor
Ejercicio1::Ejercicio1(int n, int *numeros){
    this -> n = n;
    this -> numeros = numeros;
}

//Getters y Setters
int Ejercicio1::getN(){
    return this -> n;
}
int Ejercicio1::getNumeros(){
    return this -> numeros[n];
}

void Ejercicio1::setN(int n){
    this -> n = n;
}
void Ejercicio1::setNumeros(int *numeros, int n){
    this -> numeros[n] = numeros[n];
}

//Metodos

//En este metodo, se identifica la cantidad de numeros que tendra el arreglo,
//la cual se define por el usuario en la terminal; 
//E imprime en la terminal los numeros ingresados.

void Ejercicio1::addNumeros(int *numeros, int n){
    int num;
    cout << "Ingrese los numeros que desea agregar: " << endl;
    for(int i = 0; i < n; i++){
        cin >> num;
        numeros[i] = num;
    }
    cout << "Los numeros ingresados son: " << endl;
    for(int i = 0; i < n; i++){
        num = numeros[i];
        cout << num << ",";
    }
    cout << endl;
}

//En este metodo, se calculan los cuadrados de los numeros ingresados previamente,
//y se instancia la suma, la cual comienza de 0 y va sumando los cuadrados calculados.

void Ejercicio1::sumaCuadrados(int *numeros, int n){
    int cuadrado;
    int suma = 0;
    for(int i = 0; i < n; i++){
        cuadrado = numeros[i] * numeros[i];
        suma += cuadrado;
    }
    cout << "La suma de los cuadrados de los numeros ingresados es: " << suma << endl;
}